/* 共用的 function */
import Vue from 'vue'
// import store from './store'
// import { S_Obj,L_Obj } from './s_obj.js'

// const IsDev = S_Obj.isDebug 	//開發debug
// const BASEURL = process.env.VUE_APP_BASE_API;

/* 公斤 轉 台斤 */
// export function ToTaijin(nWei) {
	// return nWei ? ToDecimal(nWei * 1.6667, 2) : 0;
// }
// export function TxtTaijin(isTaijin) {
	// return isTaijin?"台斤":"公斤";
// }


/* 去重覆 */
export function GetNoRepeat() {
	if (this.length) {
		return this.filter(function(element, index, arr){
			return arr.indexOf(element) === index;
		});
	}
	return this;
}
/** @Sample:
	GetNoRepeat.call(arr1);
*/


/* 亂數取一值 (最小值~最大值) */
export function GetRandNum(min, max) {
	max++;	// Important!!
	let num = Math.floor(Math.random()*(max-min)+min); 
	// console.error('GetRandNum-num: ', num);	// @@
  return num;
}
/** @Sample:
	let a1 = GetRandNum(1,20);		// 1~20 取一數
*/

/* 針對後端來源的日期字申多了個T */
export function ReplaceDate(v1) {
	return v1 ? v1.replace(/T/g, ' ') : '';
}

/* 取得時間差 */
export function GetDateDiff(startTime, endTime, diffType) {
	//將xxxx-xx-xx的時間格式，轉換為 xxxx/xx/xx的格式
	startTime = startTime.replace(/-/g, "/");
	endTime = endTime.replace(/-/g, "/");

	var sTime 	= new Date(startTime) 	// 開始時間
		,eTime 	= new Date(endTime)		// 結束時間
		,divNum = 1						// 作為除數的數字
	switch (diffType) {
		case "s":
			divNum = 1000;
			break;
		case "m":
			divNum = 1000 * 60;
			break;
		case "h":
			divNum = 1000 * 3600;
			break;
		case "d":
			divNum = 1000 * 3600 * 24;
			break;
		// default:
			// break;
	}
	return parseInt((eTime.getTime() - sTime.getTime()) / parseInt(divNum));
}

/** @Sample:
	var Diffday = GetDateDiff('2018/08/01', '2018/08/31', "d");
*/


/* 前端建GUID */
export function GUID() {
  var d = Date.now();
  if (typeof performance == 'object' && typeof performance.now == 'function') {
    d += performance.now(); //use high-precision timer if available
  }
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
}

/* 比對門店代號(不分大小寫) */
export function MatchShopID(v1, v2) {
	return v1.toLocaleUpperCase() == v2.toLocaleUpperCase();
	// const _key = 'Sk2'
	// if (v1 != _key || v2 != _key) {
		// return v1.toLocaleUpperCase() == v2.toLocaleUpperCase();
	// } else {
		// return v1 == v2;
	// }
}

export function IsFn(fn) {return typeof fn ==='function'}
// export function IsLogin() {
	// const uData = L_Obj.read('userData') || {}
	// IsDev && console.warn('IsLogin-uData: ', uData);	// @@
  // return !!(uData.UserCode && uData.tokenkey);
// }

export function Makeid_num(num) {
  num = num || 1;
	var possible 	= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
		,text 			= "";
	for (var i = 0; i < num; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}
export function Makeid_int(num) {
	num = num || 1;
	var possible 	= "0123456789"
		,text 			= "";
	for (var i = 0; i < num; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}
/* 
export function b64_to_utf8(str_){
  if (typeof str_ === "string") return decodeURIComponent(atob(str_));
	return "";
}
export function jParse(s){
  let pData ;
  try {
    if (typeof s === "string"){
      pData = JSON.parse(s)
    }
    pData = pData || {};
  }
  catch(e) { pData = {} }
  return pData
}
 */



/* 抓-網址參數 */
// export function GetPara1(v1) {
	// const	_hash	= location.href
		// ,_find 		= _hash.slice(_hash.indexOf('?')+1).trim()
	// // console.warn('取參數_URLParameter: ', _find);	// @@
	// if (_find) {
		// let i	 		= 0
			// ,params = _find.split("&")
			// ,nLen = params.length
		// while (i<nLen) {
			// let val = params[i].split("=");
			// if (val[0] == v1) {
				// return unescape(val[1]);
			// }
			// i++;
		// }
	// }
	// return null;
// }
/** @Use:
	GetPara1('qShopID')		// AO03
*/

// export function ReplaceMe(opts) {
	// opts || (opts = {});
	// /* 用帶進來的參數s,去取代預設參數s */	// 取代參數取代
	// for ( var i in opts ) { this[i] = opts[i]; }
// }
export function InitMe() {
	// console.log('InitMe-this: ', this);	// @@
	for ( var x in this ) { this[x] = ''; }
}

/* 四捨五入-取小數點N位 */
export function FormatFloat(x, nLen) {	// ps: 輸出文字型態
	return parseFloat(x).toFixed(nLen)
}
/** @Sample:
	FormatFloat(123.456, 2);	// '123.46'
*/
export function ToDecimal(num, pos) {		// ps: 輸出數字型態
	if (!pos) {
		return Math.round(num)
	} else {
		const size = Math.pow(10, pos);
		return Math.round(num * size) / size;
	}
}
/** @Sample:
	ToDecimal(9.8855001144409, 2)	// 9.89
*/

/* 加總,累加陣列/物件內的值 */
// export function Sum(key) {
	// const totalFocusFoodSalesAmont = pieChartRows.map( item => { return item.SaleTotal } ).reduce((a, b) => { return a + b; }, 0);

	// return _.reduce(_.pluck(this, key), function(nTotal, nCur) {
		// return (!_.isNaN(nTotal) ? nTotal : 0) + (!_.isNull(nCur) ? parseInt(nCur, 10) : 0);
	// }, 0);
// }
/** @Sample:
	Sum.call(sub0, 'sub_total');
*/

/* 字串-轉日期 */
export function Format2Date(_str) {
	if (!_str) { return ''; }
	return _str.left(4)+'/'+_str.substr(4, 2)+'/'+_str.right(2);
}

/* 取得-指定秒數後的時間 */
export function GetTimeAfter(startTime, sec) {
	var DATE1 	= '2018/01/01 '
		,sTime 	= new Date(DATE1+startTime)
		,s1 	= sTime.getTime()+sec*1000
	return new Date(s1).Format("HH:mm:ss");
}

export function Format_Time(t1, bSec) { // bSec: 預設補小時,有指定補秒數
	var ft = !bSec ? t1.padLeft(6) : t1+'00';
	return ft.left(2)+':'+ft.substring(2, 4)+':'+ft.right(2);
}
export function Format2Time(s1) {
	return s1.left(2)+':'+s1.right(2);
}
/* 金錢格式化(貨幣格式化,將數據每三位用逗號分隔 */
export function FormatMoney(value) {
	if(!value&&value!==0) return 0;
	var str = value.toString()
		,reg 	= str.indexOf(".") > -1 ? /(\d)(?=(\d{3})+\.)/g : /(\d)(?=(?:\d{3})+$)/g;
	return str.replace(reg,"$1,");
}

export function StrTime2Sec(str1) {
	var s1 = str1.replaceAll(':', '').padLeft(6);
	return s1.left(2)*3600+s1.substring(2, 4)*60+(s1.right(2)-0);
}
/* 取得時間差 */
export function Get_DiffTime_Sec(startTime, endTime) {
	var DATE1 	= '2018/01/01 '
		,s1 = Date.parse(new Date(DATE1+startTime))
		,s2 = Date.parse(new Date(DATE1+endTime))
		,isTrue = endTime > startTime
		,d1 = isTrue ? s2-s1 : s1-s2
		,s3 = new Date(d1)	// 轉換時間戳

	return new Date(s3).Format("mm:ss");
}

/* 計算含中英文字的長度 */
export function StrLen(str) {
	return str.replace(/[^\x00-\xff]/g,"xx").length;
}
/* 是否有中文字 */
export function ChkChinese(v1) {
	return new RegExp("^[\u4e00-\u9fa5]+$").test(v1);
}

/* 下拉顯示對應中文 */
// export function FindText(v1) {
	// if (!v1) { return ''; }
	// this.map((o, i) => {
		// if (o.id == v1) {
			// return o.text;
		// }
	// });
	// // for (var i=0, o; o=this[i++];) {
		// // if (o.id == v1) {
			// // return o.text;
		// // }
	// // }
	// return '';
// }

/* 用id抓Name,帶字串,只找1個 */
export function FindName(v1) {
	if (!v1) { return ''; }
	for (var i=0, o; o=this[i++];) {
		if (o.id == v1) {
			return o.name;
		// if (o == v1) {
			// return o;
		}
	}
	return '';
}
/** @Sample:
	FindName.call(ArrPay, _status)
*/

/* 用id找Name,帶字串,找所有,類似filter */
export function FindNames(str) {
	if (!str) { return ''; }

	let arr2 = []
	str.split(",").forEach((v1)=>{
		this.forEach((o)=>{
			if (o.id == v1) {
				arr2.push(o.name);
			}
		})
	})
	return arr2;
}
/** @Sample:
	FindNames.call(ArrPay, _status)
*/
/* 用Name找回id,帶陣列 */
export function FindIds(arr1) {
	if (!arr1.length) { return []; }

	let arr2 = []
	arr1.forEach((v1)=>{
		this.forEach((o)=>{
			if (o.name == v1) {
				arr2.push(o.id);
			}
		})
	})
	return arr2.sort();
}
/** @Sample:
	FindIds.call(ArrPay, _status)
*/
export function FindOne(v1) {
	if (!v1) { return ''; }
	for (var i=0, o; o=this[i++];) {
		if (o.id == v1) {
			return o;
		}
	}
	return '';
}
/** @Sample:
	FindOne.call(ArrPay, _status)
*/

export function OmitThem(str) {
	if(!Array.isArray(this)){
		throw 'OmitThem 參數錯誤!';
		return
	}

	const arrKey = str.split(",")
	return this.map((o, i) => {
		// console.log('OmitThem-o1: '+i, o);
		for (let x in o) {
			/* 移除空屬性 */
			if(!o[x]){ delete o[x]; } 	// 0,'' 皆刪
		}

		/* 刪多個屬性 */
		let exclude = new Set(arrKey)
		return Object.fromEntries(Object.entries(o).filter(e => !exclude.has(e[0])))
		// return Object.fromEntries(Object.entries(o).filter(e => e[0] != key))	// 刪1個
	})
}
/** @Sample:
	OmitThem.call(ArrPay, 'sn')
*/

/* 比對物件相同 */
// export const ObjectEqual = (x, y) => {
  // // 指向同一内存时
  // if ((typeof x == "object" && x != null) && (typeof y == "object" && y != null)) {
    // if (Object.keys(x).length !== Object.keys(y).length) {
      // return false;
    // }
    // for (var prop in x) {
      // if (y.hasOwnProperty(prop)) {
				// if (x[prop] != y[prop]) {
					// return false;
				// }
      // } else {
        // return false;
      // }
    // }
    // return true;
  // } else {
    // return false;
  // }
// }

export function SplitFor(v1) {
	return v1 ? v1.split(",") : [];
}
export function HasInclude(str, _find) {
	return str.split(",").includes(_find)
}


// const IsMobile = function() {
	// return BMobile || typeof(JSInterface) !== 'undefined';
// }

// let BMobile = IsMobile()

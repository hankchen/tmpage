;(function () {

Array.prototype.has = function(value) {
	// for (var i=0, o1; o1=this[i++];) {	/** @Error: 造成找0/false都有問題.. */
	for (var i = 0, o1, len = this.length; i < len; i++) {
		// console.warn('Array_prototype: ', o1);	// @@
		o1 = this[i];
		if (o1 == value)	return true;
		// if (o1.indexOf(value) != -1)	return true;
	}
	return false;
}

Array.prototype.del = function(key, val, bAll) {
	for (var i = 0, o1, len = this.length; i < len; i++) {
		o1 = this[i];
		if (o1[key] == val) {
			this.splice(i, 1);	// 陣列移除該位置的元素,移1個
			if (!bAll) 	break;
		}
	}
}
/** @Sample:
	FrmData.del('name', 'aa');
	FrmData.del('name', 'bb', 1);	// 刪全部的 {name: 'bb'}
*/

Array.prototype.set = function(f_key, f_val, w_key, w_val) {
	for (var i = 0, o1, len = this.length; i < len; i++) {
		o1 = this[i];
		if (o1[f_key] == f_val) {
			o1[w_key] = w_val;
			break;
		}
	}
}
/** @Sample:
	設第5隻的name為aa
	arr1.set('nid', 5, 'name', 'aa');
*/

/* 移除空元素(含空格,並將內容元素去空格 */
Array.prototype.clear = function() {
	return this.filter(function(n){ return n.trim();}) // 移除空元素
	.map(function(o, i){return o.trim();});
}

Array.prototype.pluck = function(key) {
  return this.map(function(object) { return object[key]; });
};


/* 自訂製作 string.format */
// String.prototype.format = function () {
	// var args = arguments;
	// return this.replace(/{(\d{1})}/g, function () {
		// return args[arguments[1]];
	// });
// };
/** @Sample:
var m = '取得數_{0} 還有{0}\n';
for (var i = 0; i < 5; i++) {
	menu += m.format(i);
}
*/

String.prototype.replaceAll = function (s1, s2) {
	return this.replace(new RegExp(s1, "gm"), s2);
}
/** @Sample:
var templ = '<a href="img/l/{{src}}.jpg"><img src="img/{{src}}.jpg"></a>';

for (var i = 0; i < 5; i++) {
	str += templ.replaceAll("{{src}}", data[i].src);
}
*/



/* 完整版-有周幾/星期 */
Date.prototype.Format = function (fmt) {
	var o = {
		"Y":  this.getFullYear() - 1911,	// 民國年
		"M+": this.getMonth() + 1, 	//月
		"d+": this.getDate(), 		//日
		"h+": this.getHours() % 12 == 0 ? 12: this.getHours() % 12, //小時
		"H+": this.getHours(), 		//小時
		"m+": this.getMinutes(), 	//分
		"s+": this.getSeconds(), 	//秒
		"q+": Math.floor((this.getMonth() + 3) / 3), //季度
		"S": this.getMilliseconds() //毫秒
	};
	var week = {					// 週
		"0": "日",
		"1": "一",
		"2": "二",
		"3": "三",
		"4": "四",
		"5": "五",
		"6": "六"
	};
	if (/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	}
	if (/(E+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "星期": "周"): "") + week[this.getDay() + ""]);
	}
	for (var k in o) {
		if (new RegExp("(" + k + ")").test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]): (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
}
/** @Sample:
new Date().Format("yyyy-MM-dd hh:mm:ss.S")		==> 2006-07-02 08:09:04.423
new Date().Format("yyyy-MM-dd(E) HH:mm:ss")		==> 2018-09-06(四) 19:52:23
new Date().Format("yyyy-MM-dd EE hh:mm:ss") 	==> 2009-03-10 周二 08:09:04
new Date().Format("yyyy-MM-dd EEE hh:mm:ss") 	==> 2009-03-10 星期二 08:09:04
new Date().Format("yyyy-M-d h:m:s.S") 			==> 2006-7-2 8:9:4.18
new Date().Format("Y-MM-dd") 					==> 108-08-16	// 民國年
*/



/* 字串左取 n 個字元，支持 全/半形字 區分 */
String.prototype.left = function(num,mode){
	if(!/\d+/.test(num))return(this);
	var str = this.substr(0,num);
	if(!mode) return str;
	var n = str.Tlength() - str.length;
	num = num - parseInt(n/2, 10); // 10進位
	return this.substr(0,num);
}

/** @Sample:
"EaseWe空間Spaces".left(8); 		// -> EaseWe空間
"EaseWe空間Spaces".left(8,true); 	// -> EaseWe空
*/

/* 字串右取 n 個字元 */
String.prototype.right = function(num){
	return this.slice(-num);
}
/** @Sample:
	"Hello".right(3) ==> llo
*/


/* 截取找到位置前的所有字 */
String.prototype.cutA = function(c){
	n = this.indexOf(c);
	return (n > 0) ? this.substring(0, n) : this.toString();
}
/** @Sample:
	'abcde'.cutA('c'); ==> ab
*/
/* 截取找到位置後的所有字 */
String.prototype.cutB = function(c){
	n = this.lastIndexOf(c);
	return (n > 0) ? this.substring(n+1) : this.toString();
}
/** @Sample:
	'abcde'.cutB('b'); ==> cde
*/



})();



/** @名言收錄:
	刺激與回應之間存在一段距離，
		成長和幸福的關鍵就在於我們如何利用這段距離！
*/

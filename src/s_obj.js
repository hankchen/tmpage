/** 用Session Storage,暫存本機的東西 */
/* [SessionStorage] 緩存(HTML5的內建obj) */
const S_Obj = {
	// isWebProd:	 /web/.test(location.host),
	// isStaging:	 /staging/.test(location.host),	// !isWebProd
	isDebug:	 /localhost/.test(location.host) || /^192.168/ig.test(location.host),
	save(key, v1){
		sessionStorage[key] = (typeof(v1) === 'object') ? JSON.stringify(v1) : v1;
	},
	read(key){
		const value = sessionStorage[key]
		return /{|]/.test(value) ? JSON.parse(value) : value;
		// return (typeof(value) === 'string') ? JSON.parse(value) : value;
	},
	del(key){
		sessionStorage.removeItem(key);
	},
	has(key){			// 有無
		return !!sessionStorage.getItem(key);
	},
	clear(){
		sessionStorage.clear(); //刪除所有資料
	},

};

/* [LocalStorage] 緩存(HTML5的內建obj) */
const L_Obj = {
	has(key){			// 有無
		return !!localStorage.getItem(key);
	},
	get1(v1){
		return typeof(v1) == 'object' ? JSON.stringify(v1) : v1;
	},
	get2(v1){
		return /{|]/.test(v1) ? JSON.parse(v1) : v1;
		// return (typeof(v1) === 'string') ? JSON.parse(v1) : v1;
	},
	save(key, v1){
		localStorage[key] = this.get1(v1);
	},
	read(key){
		const value = localStorage[key]
		return this.get2(value);
	},
	save64(key, v1){
		localStorage[key] = window.btoa(encodeURIComponent(this.get1(v1)));
	},
	read64(key){
		// const v1 = this.read(key);
		const v1 = localStorage[key]
		if (v1) {
			const v2 = decodeURIComponent(window.atob(v1))
			return this.get2(v2);

		} else {
			return null;
		}
	},
	del(key){
		localStorage.removeItem(key);
	},
	clear(){
		localStorage.clear();
	},
/* 
	set: function(name, key, value) {		// 設某一支
		var obj1 = this.read(name) || {};
		obj1[key] = value;
		this.save(name, obj1);
	},
	get: function(name, key) {				// 讀某一支
		var obj1 = this.read(name) || {};
		return obj1[key];
	},
	put: function(name, value) {			// 陣列追加
		var arr1 = this.read(name) || [];
		arr1.push(value);
		this.save(name, arr1);
	},
	remove: function(name, value) {			// 陣列移除該元素(自動找值)
		var arr1 = this.read(name);
		if (!arr1) { return; }

		if (_.isObject(arr1)) {
			arr1 = _.omit(arr1, value);
		} else {
			const i = _.indexOf(arr1, value);
			(i != -1) && arr1.splice(i, 1);
		}

		this.save(name, arr1);
	},
	hasIt: function(name, value) {			// 確認陣列有無該內容
		var arr1 = this.read(name) || [];
		for (var i=0, item; item=arr1[i++];) {
			// console.warn('hasIthasIt: ', item);	// @@
			if (item == value)	return true;
		}
		return false;
	},
 */
};


!S_Obj.isDebug && S_Obj.clear();		// 確保F5後,一律先讀後端

export { S_Obj, L_Obj }
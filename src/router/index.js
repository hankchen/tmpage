import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  }, {
    path: '/bestpartner1',
    name: 'bestpartner1',
    component: () => import('@/views/bestpartner/tmppage1.vue')
  }, {
    path: '/bestpartner2',
    name: 'bestpartner2',
    component: () => import('@/views/bestpartner/tmppage2.vue')
  }, {
    path: '/hexagram1',
    name: 'hexagram1',
    component: () => import('@/views/hexagram/tmppage1.vue')
  }, {
    path: '/hexagram2',
    name: 'hexagram2',
    component: () => import('@/views/hexagram/tmppage2.vue')
  }, {
    path: '/noble1',
    name: 'noble1',
    component: () => import('@/views/noble/tmppage1.vue')
  }, {
    path: '/noble2',
    name: 'noble2',
    component: () => import('@/views/noble/tmppage2.vue')
  }, {
    path: '/nobleres1',
    name: 'nobleres1',
    component: () => import('@/views/noble/tmpresult1.vue')
  }, {
		path: '/updatePrice',
    name: 'updatePrice',
    component: () => import('@/views/UpdatePrice.vue')
  }, {
		path: '/todayPrice',
    name: 'todayPrice',
    component: () => import('@/views/TodayPrice.vue')
  }, {
		path: '/todayPrice2',
    name: 'todayPrice2',
    component: () => import('@/views/TodayPrice2.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router

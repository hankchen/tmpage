/* 共用的 vue-methods */
/* 限制 字數 */
export function maxlen(e, max) {
	const v1 = e.target.value
	let v2 = v1
	if (v1.length > max) {
		v2 = v1.substring(0, max)
	}
	e.target.value = v2
}
/** @Sample:
	@keyup="maxlen($event, 4)"
*/

/* 限制 只能輸入 數字 */
export function vald_Num(e) {
	let key = e.which;
	// /* 排除 後退/刪除/左/右鍵 */
	// if (!(key == 8 || key == 46 || key == 37 || key == 39)) {
		e.target.value = e.target.value.replace(/\D/g, '');
	// }
}
/** @Sample:
	@blur="vald_Num"
*/

/* 右鍵-清空還原 */
export function inpRclick(e) {
	e.preventDefault(), e.stopPropagation();
	const me	= e.target ,v1 = me.value
	// console.warn('inpRclick-v1,me: '+v1, me);	// @@
	if (v1) {
		me.setAttribute("hData", v1);
		me.value = '';
	} else {
		me.value = me.getAttribute("hData");
	}
}
/* 自動全選 */
export function selAll(event) {
	// console.warn('sel_All-event: ', event);	// @@
	event.target.select()
}
export function showMsg(v1) {
	return this.$bvModal.msgBoxOk(v1);
}
export function goto(v1) {
	// console.warn('goto-v1: ', v1);	// @@
	this.$router.push(v1).catch(e => {});
}

/* 複製元素內容 */
export function CopyText(e) {
	// console.warn('CopyText-target: ', e.target);	// @@
	Copy2clip(e.target.outerText);
	this.$toast('已複製');	// PS: 不能直接調用!!
}
/* 剪貼簿複製文字 */
export function Copy2clip(s, bMlert) {
	const clip_area = document.createElement('textarea');
	clip_area.textContent = s;

	document.body.appendChild(clip_area);
	clip_area.select();

	const isOK = document.execCommand('copy');
	clip_area.remove();

	if (bMlert && isOK) {
		// Vue.prototype.$toast(s+' 已複製');
		// Vue.prototype.$toast({
		this.$toast({
			message: ' 已複製',
			type: 'success',
			duration: 800
		})
	}
}


/* mobile|EMail 變*號 */
export function formatStar(v1) {
	if (!v1) { return ''; }
	const isMail = /@/.test(v1)
	if (isMail) {
	 let f1 = '@'
			,c1 = '*'
	 let former = v1.cutA(f1)
		,nLen1 = former.length
		,latter 	= v1.cutB(f1)
		,nLen2 = latter.length
	 return former.slice(0, 4).padEnd(nLen1,c1)+f1+latter.slice(5).padStart(nLen2,c1)

	} else {
		const reg = /^(\d{2})\d*(\d{3})$/
		return v1.replace(reg, '$1*****$2')
	}
}

import Vue from 'vue'
import '@/utils/JSprototype'
import App from './App.vue'
import router from './router'
import store from './store'
// import bootstrap from 'bootstrap'
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

/** @Sample:
import Alert from 'bootstrap/js/dist/alert'
// or, specify which plugins you need:
import { Tooltip, Toast, Popover } from 'bootstrap'
 */


import { Toast } from 'vant';
import 'vant/lib/index.css';

Vue.use(Toast);
Vue.prototype.$toast = Toast;
Vue.prototype.$bus = new Vue();

import '@/styles/index.scss';

Vue.config.productionTip = false

/* 
router.afterEach((to, from, next) => {
	const curHash = to.path.substr(1)
	// console.error('scrollBehavior-curHash: ', curHash);	// @@
	store.commit('SET_CURPAGE', curHash);
})
 */

// store.dispatch('fetchAllData').then(response => {
	new Vue({
		router,
		store,
		render: h => h(App)
	}).$mount('#app')
// }).catch(err => {
	// console.error('Error: ', err);	// @@
// })


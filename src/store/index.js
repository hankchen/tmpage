import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import { AjaxAct } from '@/utils/ajax1'


export default new Vuex.Store({
  state: {
		CurStep: 0,			// 0: main, 1: step1 ...
		// CurStep: 3,					// @@deldebug

		gData: {
			usprice: 0,		// USDT匯率
      buy1: 0,			// 黄金條塊-買入		// goldpriceB
      sell1: 0,			// 黄金條塊-賣出		// goldpriceBS
      buy2: 0,			// 飾金-買入				// goldprice
      sell2: 0,			// 飾金-賣出				// goldpriceS
		},
		// 基本資訊
		baseInfo: {
			app: '',
			lang: 'tw',	// 語系 (預設值：'tw')
			// urlImg: 'static/img/',	// NG!
			urlImg: '',
			urlIPC: '',
			urlLang: '',
		},
/* 
{
	app: 'fuxi_hexagram',
	lang: 'tw',
	urlImg: '/static/playlife_dapp/divination/fuxi_hexagram/img/',
	urlIPC: '/static/playlife_dapp/divination/fuxi_hexagram/img/pc/',
	urlLang: '/static/playlife_dapp/divination/fuxi_hexagram/img/tw'
}
 */
  },
	getters: {
	/** @Sample:
		this.$store.getters.urlImg
	*/
		baseUrl : () => {
			return URL_IMG
		},
		// getUsprice: (state) => {
			// return state.gData.usprice;
		// },
		urlImg: (state) => {
			return state.baseInfo.urlImg;
		},
		urlIPC: (state) => {
			return state.baseInfo.urlIPC;
		},
		urlLang: (state) => {
			return state.baseInfo.urlLang;
		},
	},
  mutations: {
/** @Sample:
		this.$store.commit('setStep', 1)
*/
		setStep(state, n1) { state.CurStep = n1 },

		SET_USPRICE(state, data) { state.gData.usprice = data },
		// 黄金條塊-買入
		SET_BUY1(state, data) { state.gData.buy1 = data },
		// 黄金條塊-賣出
		SET_SELL1(state, data) { state.gData.sell1 = data },
		// 飾金-買入
		SET_BUY2(state, data) { state.gData.buy2 = data },
		// 飾金-賣出
		SET_SELL2(state, data) { state.gData.sell2 = data },
		// 設定語系 (只接受 tw, cn, en, jp)
		// setI18nLocate(state, input) {
			// const lang = input.toLowerCase()
			// const whiteList = {'tw': true, 'cn': true, 'en': true, 'jp': true}
			// if (whiteList[lang])  { i18n.locale = lang ; state.baseInfo.lang = lang }
			// if (!whiteList[lang]) { i18n.locale = 'tw' ; state.baseInfo.lang = 'tw' }
		// },
  },
  actions: {
		/* 取得USDT匯率 */
		async fetchUsprice({commit}) {
		// async fetchUsprice({ commit, state }, payload) {
			let a1 	= await AjaxAct("usprice")
				,data	= a1.usprice[0].usprice
			// console.warn('取得USDT匯率-a1: ', a1);	// @@
			commit('SET_USPRICE', data)
		},
		/* 取得-黄金條塊-買入 */
		async fetchBuy1({commit}) {
			let a1 = await AjaxAct("goldpriceB")
				,data	= a1.goldpriceB[0].goldpriceB
			commit('SET_BUY1', data)
		},
		/* 取得-黄金條塊-賣出 */
		async fetchSell1({commit}) {
			let a1 = await AjaxAct("goldpriceBS")
				,data	= a1.goldpriceBS[0].goldpriceBS
			commit('SET_SELL1', data)
		},
		/* 取得-飾金-買入 */
		async fetchBuy2({commit}) {
			let a1 	= await AjaxAct("goldprice")
				,data	= a1.goldprice[0].goldprice
			// console.warn('取得飾金買入-a1: ', a1);	// @@
			commit('SET_BUY2', data)
		},
		/* 取得-飾金-賣出 */
		async fetchSell2({commit}) {
			let a1 	= await AjaxAct("goldpriceS")
				,data	= a1.goldpriceS[0].goldpriceS
			commit('SET_SELL2', data)
		},
		/* 取得-所有資料 */
		async fetchAllData({dispatch}) {
      await dispatch('fetchUsprice')			// 取得USDT匯率
      await dispatch('fetchBuy1')
      await dispatch('fetchSell1')
      await dispatch('fetchBuy2')
      await dispatch('fetchSell2')
			return new Promise(resolve => resolve())
		},

  },
})

/* 
store.state.baseInfo = GetURLPara()
console.error('Store-baseInfo: ', store.state.baseInfo);	// @@


function GetURLPara() {
	let one = {}
	
	const arr1 = location.pathname.split('/')
	if (arr1.length) {
		const txt1 	= arr1[1]
			,app 			= arr1[5]
			,isTaiwan	= txt1.lastIndexOf('t') != -1
			,lang 		= isTaiwan ? 'tw' : 'cn'
			,urlImg		= `${URL_IMG}${app}/img/`

		one.app = app
		one.lang = lang
		one.urlImg = urlImg
		one.urlIPC = urlImg+(IsPC?'pc/':'')
		one.urlLang = `${urlImg}${lang}`
	}
  return one
}
 */